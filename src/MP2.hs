import Data.Char

-- warm-up exercises

any' :: (a -> Bool) -> [a] -> Bool
any' p = foldr ((||) . p) False

all' :: (a -> Bool) -> [a] -> Bool
all' p = foldr ((&&) . p) True

compose :: [a -> a] -> (a -> a)
compose = foldr (.) id

cycle' :: [a] -> [a]
cycle' l = foldr (:) (cycle' l) l

scanr' :: (a -> b -> b) -> b -> [a] -> [b]
scanr' f v = foldr (\x ys@(y:_) -> f x y : ys) [v]

scanl' :: (b -> a -> b) -> b -> [a] -> [b]
scanl' f v = foldl (\ys x -> ys ++ [f (last ys) x]) [v]

inits :: [a] -> [[a]]
inits = ([]:) . foldr (\x xs -> map (x:) ([]:xs)) []

tails :: [a] -> [[a]]
tails = foldr (\x ys@(y:_) -> (x:y):ys) [[]]

-- trickier exercises (may need to fold with a tuple!)

minmax :: (Ord a) => [a] -> (a,a)
minmax (x:xs) = foldr f (x,x) xs
  where f y (mn,mx) = (min mn y, max mx y)

gap :: (Eq a) => a -> a -> [a] -> Maybe Int
gap x y l = let (_,i,j) = foldl f (0,-1,-1) l
                  where f (idx,i,j) w
                          | i < 0 && w == x = (idx+1,idx,j)
                          | i >= 0 && j < 0 && w == y = (idx+1,i,idx)
                          | otherwise = (idx+1,i,j)
            in if i>=0 && j>=0 then Just (j-i) else Nothing

evalExpr :: String -> Int
evalExpr = fst . foldl step (0,(+))
  where step (r,op) x
          | '0' <= x && x <= '9' = (r `op` (ord x - ord '0'), op)
          | otherwise = (r, case x of '+' -> (+)
                                      '-' -> (-))

words' :: String -> [String]
words' = fst . foldr step ([], False)
  where step x ([],p)
          | x == ' ' = ([],True)
          | otherwise = ([[x]],False)
        step x (words@(w:ws),p)
          | x /= ' ' && not p = ((x:w):ws,False)
          | x /= ' ' && p = ([x]:words,False)
          | otherwise = (words, True)

dropWhile' :: (a -> Bool) -> [a] -> [a]
dropWhile' p = fst . foldr f v
  where
    f x (ys,xs) = (if p x then ys else x:xs, x:xs)
    v = ([],[])

-- 1. join - basic list processing
join :: a -> [[a]] -> [a]
join sep = foldr step []
  where step x [] = x
        step x y = x ++ [sep] ++ y

-- 2. countIf - basic list processing
countIf :: (a -> Bool) -> [a] -> Int
countIf p = foldr (\x y -> if p x then y+1 else y) 0

-- 3. unzip - basic list processing
unzip' :: [(a,b)] -> ([a], [b])
unzip' = foldr (\(x,y) (xs,ys) -> (x:xs, y:ys)) ([], [])

-- 4. run-length encoding
runLengthEncode :: String -> [(Int,Char)]
runLengthEncode = foldr step []
  where step c [] = [(1,c)]
        step c lst@((n,l):rest)
          | c == l = (n+1,c):rest
          | otherwise = (1,c):lst

-- 5. run-length decode
runLengthDecode :: [(Int,Char)] -> String
runLengthDecode = foldl (\s (n,c) -> s ++ replicate n c) ""

-- 6. vigenere encryption
vigenere :: String -> String -> String
vigenere "" = error "Key text cannot be blank"
vigenere k = foldr step id (cycle k)
  where step x f "" = ""
        step x f (p:ps)
          | isLetter p = encrypt p : f ps
          | otherwise  = p : step x f ps
          where encrypt p = chr (((ord (toUpper p) - ord 'A'
                                    + ord (toUpper x) - ord 'A') 
                                   `rem` 26) + ord 'A')
