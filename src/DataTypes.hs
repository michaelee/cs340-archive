import Data.List
-- import System.Random

data MyBool = Yes | No
  deriving (Show)

infixl 5 <<
(<<) :: Int -> Int -> MyBool
(<<) x y
  | x < y = Yes
  | otherwise = No

showBool :: MyBool -> String
showBool Yes = "Yes"
showBool No = "No"

type YesNo = Bool

foo :: Int -> YesNo
foo x = x < 10

type ListOInts = [Int]

sumOInts :: ListOInts -> Int
sumOInts = sum

-- redefining the List
           
data List a = Cons a (List a) | Nil
  deriving (Show)

-- Cons 1 $ Cons 2 $ Cons 3 Nil -- is a list

llength :: List a -> Int
llength Nil = 0
llength (Cons x xs) = 1 + llength xs

infixr 5 +++
(+++) :: List a -> List a -> List a
(+++) Nil xs = xs
(+++) (Cons x xs) ys = Cons x (xs +++ ys)

-- motivate Maybe type

-- findIndex (=='e') "hello"
-- findIndex (=='z') "hello"

data Perhaps a = Actually a | Nada
  deriving (Show)

findIndex' :: (a -> Bool) -> List a -> Perhaps Int
findIndex' p xs = f xs 0
  where f Nil _ = Nada
        f (Cons x xs) i
          | p x = Actually i
          | otherwise = f xs (i+1)

-- findIndex' (=='e') (Cons 'h' (Cons 'e' (Cons 'l' Nil)))

quadRoots :: (Ord a, Floating a) => a -> a -> a -> Maybe (a,a)
quadRoots a b c
  | disc >= 0 = Just ((-b+sqrt disc)/(2*a),(-b-sqrt disc)/(2*a))
  | otherwise = Nothing
  where disc = b^2 - 4*a*c

-- quadRoots 1 3 2
-- quadRoots 1 1 1

-- pattern matching and "extracting" values from Just
quadRootsWithError :: (Ord a, Floating a) => a -> a -> a -> (a,a)
quadRootsWithError a b c = case quadRoots a b c of Just (r1,r2) -> (r1,r2)
                                                   Nothing -> error "No real roots!"

-- found in module Data.Maybe
fromJust :: Maybe a -> a
fromJust (Just x) = x

-- common error - can't work, because (==) requires content of Maybe to be Eq (see :info Maybe)
-- bar :: Maybe a -> Bool
-- bar = (== Nothing)

-- This is ok. Also found in module Data.Maybe
isNothing :: Maybe a -> Bool
isNothing Nothing = True
isNothing _ = False
