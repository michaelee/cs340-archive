import Data.Char

-- # Currying, partial application, and sectioning

map' :: (a -> b) -> [a] -> [b]
map' f [] = []
map' f (x:xs) = f x : map' f xs

filter' :: (a -> Bool) -> [a] -> [a]
filter' p [] = []
filter' p (x:xs)
  | p x = x : filter' p xs
  | otherwise = filter' p xs

-- # Lambda functions (vs. partial application)

-- map (\x -> 2*x) [1..4]
-- map (2*) [1..4]
-- map (\(a,b) -> sqrt (a^2 + b^2)) [(1,0), (2,2), (5,5)]
-- filter (\x -> reverse x == x) (words "madam I'm adam")

flip' :: (a -> b -> c) -> b -> a -> c
flip' f = \x y -> f y x

-- # Function application operators

k2c k = k - 273
c2f c = c * 9 / 5 + 32
f2h f
  | f < 0 = "too cold"
  | f > 100 = "too hot"
  | otherwise = "survivable"

-- f2h $ c2f $ k2c 200

-- filter (\x -> reverse x == x) $ words "madam I'm adam"

-- # Function composition
comp :: (b -> c) -> (a -> b) -> (a -> c)
comp g f = \x -> g $ f x

-- (comp (== 0) (`rem` 2)) 5
-- (comp length (filter isUpper)) "Hello World"

-- prefer (.) as infix version of (comp)

-- filter ((== 0) . (`rem` 2)) [1..10]
-- filter ((== "survivable") . f2h . c2f . k2c) [100, 20, 300, 270, 90]

-- # Point-free (i.e., argument free) style  

-- even' x = (== 0) . (`rem` 2) $ x
-- even' = (== 0) . (`rem` 2)
-- k2h k = f2h $ c2f $ k2c k
-- k2h = f2h . c2f . k2c
          
-- # A few more HOFs

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith' _ [] _ = []
zipWith' _ _ [] = []
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

fibonacciSeq :: Num a => [a]
fibonacciSeq = 1 : 1 : zipWith' (+) fibonacciSeq (tail fibonacciSeq)

-- # Folds

sum' :: (Num a) => [a] -> a
sum' [] = 0
-- sum' (x:xs) = x + sum' xs
sum' (x:xs) = (+) x $ sum' xs

product' :: (Num a) => [a] -> a
product' [] = 1
-- product' (x:xs) = x * product' xs
product' (x:xs) = (*) x $ product' xs
                  
stringify :: (Show a) => [a] -> String
stringify [] = ""
-- stringify (x:xs) = show x ++ stringify xs
stringify (x:xs) = ((++) . show) x (stringify xs)

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f x [] = x
foldr' f x (y:ys) = f y $ foldr' f x ys

sum'' = foldr' (+) 0
product'' = foldr' (*) 1

stringify' :: (Show a) => [a] -> String
stringify' = foldr' ((++) . show) ""

map'' f lst = foldr ((:) . f) [] lst

filter'' f lst = foldr iter [] lst
  where iter x xs | f x = x : xs
                  | otherwise = xs

(+++) l1 l2 = foldr' (:) l2 l1

foldr1' :: (a -> a -> a) -> [a] -> a
foldr1' f [x] = x
foldr1' f (x:xs) = f x $ foldr1' f xs

sum''' = foldr1' (+)

maximum' :: (Ord a) => [a] -> a
maximum' = foldr1' max

-- try foldr1' (-) [10,5,1]
-- try foldr1' (/) [100,5,20]

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f x [] = x
foldl' f x (y:ys) = foldl' f (f x y) ys

reverse' = foldl' (flip (:)) []

foldl1' :: (a -> a -> a) -> [a] -> a
foldl1' f (x:xs) = foldl f x xs
-- foldl1' f (x:[]) = x
-- foldl1' f (x:y:xs) = foldl1' f $ f x y : xs

-- try foldl1' (-) [10,5,1]
-- try foldl1' (/) [100,5,20]

length'' :: [a] -> Int
length'' = foldr f 0
  where f x r = 1 + r
  

-- # Folding infinite lists

-- try foldr (||) False $ map even [1..]
-- try foldl (||) False $ map even [1..]

fl :: (b -> a -> b) -> b -> [a] -> b
fl f v l = foldr h k l v
  where
    k = id
    h = \x ys -> \w -> ys (f w x)

zip' :: [a] -> [b] -> [(a,b)]
-- zip' [] [] = []
-- zip' _ [] = []
-- zip' [] _ = []
-- zip' (x:xs) (y:ys) = (x,y) : zip' xs ys

-- zip' xs ys = fst $ foldl step ([],ys) xs
--   where step (ts,[]) x = (ts,[])
--         step (ts,y:ys) x = (ts++[(x,y)], ys)

-- zip' xs ys = foldr step g xs ys
--   where step x f = \v -> case v of [] -> []
--                                    (y:ys) -> (x,y) : f ys
--         g = \_ -> []

zip' xs = foldr step g xs
  where step x f [] = []
        step x f (y:ys) = (x,y) : f ys
        g _ = []

average :: (Fractional a) => [a] -> a
average xs = let (sum,n) = foldr (\x (sum,n) -> (x+sum,n+1)) (0,0) xs
             in sum / n
