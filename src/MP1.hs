import Data.Char

cycleN :: Int -> [a] -> [a]
cycleN 0 _ = []
cycleN n xs = xs ++ cycleN (n-1) xs

countLessThan :: (Ord a) => a -> [a] -> Int
countLessThan _ [] = 0
countLessThan x (y:ys) = (if y < x then 1 else 0) + countLessThan x ys

removeAll :: (Eq a) => [a] -> [a] -> [a]
removeAll [] ys = ys
removeAll xs [] = []
removeAll xs (y:ys) | xs `has` y = removeAll xs ys
                    | otherwise  = y : removeAll xs ys
  where [] `has` y = False
        (x:xs) `has` y = x == y || xs `has` y

join :: a -> [[a]] -> [a]
join sep [] = []
join sep (x:[]) = x
join sep (x:xs) = x ++ [sep] ++ join sep xs

unzip' :: [(a,b)] -> ([a], [b])
unzip' [] = ([], [])
unzip' ((x,y):rest) = let (xs,ys) = unzip' rest in
                        (x:xs, y:ys)

runLengthEncode :: String -> [(Int,Char)]
runLengthEncode "" = []
runLengthEncode (x:xs) = encode x 1 xs
  where encode x n [] = [(n,x)]
        encode x n (y:ys)
          | x == y = encode x (n+1) ys
          | otherwise = (n,x) : encode y 1 ys

runLengthDecode :: [(Int,Char)] -> String
runLengthDecode [] = ""
runLengthDecode ((n,c):rest) = replicate n c ++ runLengthDecode rest

vigenere :: String -> String -> String
vigenere "" _ = ""
vigenere _ "" = error "Key text cannot be blank"
vigenere (p:ps) key@(k:ks)
  | isLetter p && isLetter k =
      chr (((ord (toUpper p) - ord 'A' + ord (toUpper k) - ord 'A')
            `rem` 26) + ord 'A')
      : vigenere ps (ks ++ [k])
  | otherwise = p : vigenere ps key
