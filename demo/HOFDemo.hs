-- Currying, partial application, and sectioning


-- Basic HOFs

-- map' :: (a -> b) -> [a] -> [b]
-- filter' :: (a -> Bool) -> [a] -> [a]


-- Lambda functions (vs. partial application)


-- Function application operator ($)

k2c k = k - 273
c2f c = c * 9 / 5 + 32
f2h f
  | f < 0 = "too cold"
  | f > 100 = "too hot"
  | otherwise = "survivable"


-- Function composition

-- comp :: (b -> c) -> (a -> b) -> (a -> c)

                
-- Point-free (i.e., argument free) style  

    
-- A few more HOFs

-- zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]

-- Folds (reduction)

-- sum' :: (Num a) => [a] -> a
-- product' :: (Num a) => [a] -> a
-- stringify :: (Show a, Num a) => [a] -> String

-- reduce :: (a -> b -> b) -> b -> [a] -> b

-- foldr' :: (a -> b -> b) -> b -> [a] -> b

-- foldl' :: (b -> a -> b) -> b -> [a] -> b

                
-- Scans
